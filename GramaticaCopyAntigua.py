# 3liezerSong
import ply.lex as lex
errores = []

reservadas = {
    'cond'    : 'econd',
    'instr'   : 'einstr',
    'imprimir': 'eimprimir',

    'int'    : 'eint',
    'double' : 'edouble',
    'string' : 'estring',
    'boolean': 'eboolean',
    'char'   : 'echar',

    'while' : 'ewhile',
    'do'    : 'edo',
    'for'   : 'efor',
    'if'    : 'eif',
    'else'  : 'eelse',

    'true'  : 'etrue',
    'false' : 'efalse',
    'null'  : 'enull',
}

t_epotencia = r'\*\*'
t_edividir = r'/'
t_emultiplicar = r'\*'
t_emodulo = r'\%'
t_esumar = r'\+'
t_eresta = r'\-'

t_ecoma = r','
t_edspuntos = r':'
t_eptcoma = r';'

t_eigual = r'='
t_eigualigual = r'=='
t_diferente = r'!='
t_emenor = r'<'
t_emayor = r'>'
t_emenorigual = r'<='
t_emayorigual = r'>='

t_eor = r'\|\|'
t_eand = r'&&'
t_enot = r'!'

t_eparabre = r'\('
t_eparcierra = r'\)'
t_ecorabre = r'\['
t_ecorcierra = r'\]'
t_ellaabre = r'\{'
t_ellacierra = r'\}'

t_eincremento = '\+\+'
t_edecremento = '\-\-'

# TOKENS

tokens = [
    'epotencia',     # **
    'edividir',      # /
    'emultiplicar',  # *
    'emodulo',       # %
    'esumar',        # +
    'eresta',        # -
    'eigual',        # =
    'eigualigual',   # ==
    'diferente',     # !=
    'emenor',        # <
    'emayor',        # >
    'emenorigual',   # <= 
    'emayorigual',   # >=
    'eor',           # ||
    'eand',          # &&
    'enot',          # !
    'eparabre',      # (
    'eparcierra',    # )
    'ecorabre',      # [
    'ecorcierra',    # ]
    'ellaabre',      # {
    'ellacierra',    # }
    'eincremento',   # ++
    'edecremento',   # --
    'eentero',
    'edecimal',
    'id',
] + list(reservadas.values())

def t_edecimal(t): #Decimales
    r'\d+\.\d+'
    try:
        t.value = float(t.value)
    except ValueError:
        print("Valor flotante demasiado grande %d", t.value)
        t.value = 0
    return t

def t_eentero(t): #Entero
    r'\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        print("Valor entero demasiado largo %d", t.value)
        t.value = 0
    return t

def t_id(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reservadas.get(t.value.lower(), 'id')
    return t


def t_ecomentario(t): #Comentario linea unica
    r'\#.*\n'
    t.lexer.lineno += 1


def t_ecomentarios(t): #Comentario muchas lineas
    r'\#\&(.|\n)*?\&\#'
    t.lexer.lineno += t.value.count('\n')

#ignorar todos estos...
t_ignorar = " \t\r" #Ignorar

def t_enuevalinea(t):   #Nueva linea
    r'\n+'
    t.lexer.lineno += t.value.count("\n")


def t_eerror(t): #Errores
    print("Caracter No Soportado :( '%s'" % t.value[0])
    errores.append(Error("Lexico", "Caracter " + t.value[0] + " no pertenece al lenguaje EPK ", t.lexer.lineno, find_column(input, t)))
    t.lexer.skip(1)

def find_column(inps, token):
    line_start = inps.rfind('\n', 0, token.lexpos) + 1
    return (token.lexpos - line_start) + 1




#ANALIZADOR LEXICO
lexer = lex.lex()

#PRECEDENCIA

# ----------MASTER SINTACTICO-----------

from Enums.TipoOperacion import OperadorAritmetico, OperadorRelacional, OperadorLogico
from Enums.TipoValor import TIPO


def p_inicio(t):
    'INICIO : LINSTRUCCIONES'

def p_linstrucciones(t):
    'LINSTRUCCIONES : LINSTRUCCIONES CUERPO'

def p_linstrucciones2(t):
    'LINSTRUCCIONES : CUERPO'

def p_cuerpo(t):
    '''CUERPO : DECLARACION
              | ASIGNACION
              | SENTENCIAS
              | IMPRIMIR
    '''
def p_declaracion(t):
    'DECLARACION : TIPODATO id edspuntos EXPRESION eptcoma'

def p_declaracion2(t):
    'DECLARACION : TIPODATO id eptcoma'

def p_tipo(t):
    '''TIPODATO : eint
                | edouble
                | estring
                | echar
                | eboolean
    '''
    if t[1].upper() == 'INT': t[0] = TIPO.ENTERO
    elif t[1].upper() == 'STRING': t[0] = TIPO.CADENA
    elif t[1].upper() == 'CHAR': t[0] = TIPO.CARACTER
    elif t[1].upper() == 'DOUBLE': t[0] = TIPO.DECIMAL
    elif t[1].upper() == 'BOOLEAN': t[0] = TIPO.BOOLEAN
    else: pass

def p_asignacion(t):
    'ASIGNACION : id eigualigual EXPRESION eptcoma'

#/*Aritmetica*/
#/*Relacional*/
#/*Logicos*/
def p_expresion(t):
    ''' EXPRESION : EXPRESION esuma EXPRESION
                  | EXPRESION eresta EXPRESION
                  | EXPRESION esuma EXPRESION
                  | EXPRESION edivision EXPRESION
                  | EXPRESION emodulo EXPRESION
                  | EXPRESION epotencia EXPRESION

                  | EXPRESION emenorigual EXPRESION
                  | EXPRESION emayorigual EXPRESION
                  | EXPRESION eigualigual EXPRESION
                  | EXPRESION ediferente EXPRESION
                  | EXPRESION emenor EXPRESION
                  | EXPRESION emayor EXPRESION
    
                  | EXPRESION eand EXPRESION
                  | EXPRESION eor EXPRESION,
    '''

def p_sentencias(t):
    '''SENTENCIAS : IFF
                  | WHILEE
                  | DOWHILEE
                  | FORR
                  | IMPRIMIR
    '''

def p_instruccionIf(t):
    '''IFF : eif edspuntos eparabre CONDICION ecoma CUERPOSENTENCIA eparcierra
           | eif edspuntos eparabre CONDICION ecoma CUERPOSENTENCIA eparcierra ELSEE,
           | eif edspuntos eparabre CONDICION ecoma CUERPOSENTENCIA eparcierra eelse IIF'''

def p_instruccionElse(t):
    'ELSEE : eelse edspuntos eparabre CUERPOSENTENCIA eparcierra'

def p_condicion(t):
    'CONDICION : econd edspuntos eparabre EXPRESION eparcierra'

def p_instruccionWhile(t):
    'WHILEE : ewhile edspuntos eparabre CONDICION ecoma CUERPOSENTENCIA eparcierra'

def p_instruccionDoWhile(t):
    'DOWHILEE : edo edspuntos eparabre CUERPOSENTENCIA ecoma CONDICIONDOWHILE eparcierra'

def p_condicionDoWhile(t):
    'CONDICIONDOWHILE : ewhile edspuntos eparabre CONDICION eparcierra'

def p_instruccionFor(t):
    'FORR : efor edspuntos eparabre CONDICIONFOR eparcierra ecoma CUERPOSENTENCIA'

def p_condicionFor(t):
    'CONDICIONFOR : econd edspuntos ASIGNACION EXPRESION eptcoma PASO3'

def p_pasoFor(t):
    '''PASO3 : Identificador Igual EXPRESION 
             | Identificador Suma Suma 
             | Identificador Menos
    '''

def p_imprimir(t):
    'IMPRIMIR : eimprimir EXPRESION eptcoma'


























'''
# MI GRAMATICA :)

INICIO := LINSTRUCCIONES,

LINSTRUCCIONES := LINSTRUCCIONES CUERPO
               := CUERPO,

CUERPO := DECLARACION
        | ASIGNACION
        | SENTENCIAS,

DECLARACION := TIPODATO LISTAIDENTIFICADORES edspuntos EXPRESION eptcoma,
            | TIPODATO LISTAIDENTIFICADORES eptcoma,

ASIGNACION := id edspuntos EXPRESION eptcoma
           := id eincremento eptcoma
           := id edecremento eptcoma,

SENTENCIAS := IFF
           | WHILEE
           | DOWHILEE
           | FORR
           | IMPRIMIR,

IMPRIMIR: = eimprimir CONDICION eptcoma,

EXPRESION := /*Aritmetica*/
          EXPRESION esuma EXPRESION
        | EXPRESION eresta EXPRESION
        | EXPRESION esuma EXPRESION
        | EXPRESION edivision EXPRESION
        | EXPRESION emodulo EXPRESION
        | EXPRESION epotencia EXPRESION
            /*Relacional*/
        | EXPRESION emenorigual EXPRESION
        | EXPRESION emayorigual EXPRESION
        | EXPRESION eigualigual EXPRESION
        | EXPRESION ediferente EXPRESION
        | EXPRESION emenor EXPRESION
        | EXPRESION emayor EXPRESION
            /*Logicos*/
        | EXPRESION eand EXPRESION
        | EXPRESION eor EXPRESION,

IFF := eif edspuntos eparabre CONDICION ecoma CUERPOSENTENCIA eparcierra
    | eif edspuntos eparabre CONDICION ecoma CUERPOSENTENCIA eparcierra ELSEE,
    | eif edspuntos eparabre CONDICION ecoma CUERPOSENTENCIA eparcierra eelse IIF,

ELSEE := eelse edspuntos eparabre CUERPOSENTENCIA eparcierra,

CONDICION := econd edspuntos eparabre EXPRESION eparcierra,

CUERPOSENTENCIA:= ,

WHILEE := ewhile edspuntos eparabre CONDICION ecoma CUERPOSENTENCIA eparcierra,

DOWHILEE := edo_ edspuntos eparabre CUERPOSENTENCIA ecoma CONDICION eparcierra,

FORR := efor edspuntos eparabre CONDICION ecoma CUERPOSENTENCIA eparcierra,

'''
