from Ambito.Instrucciones import Instrucciones
from Ambito.Arbol import NodoArbol
from Tablas_simbolos.Error import Error
from Enums.TipoOperacion import OperadorRelacional
from Enums.TipoValor import TIPO


class Relacional(Instrucciones):
    def __init__(self, operador, OpIzq, OpDer, fila, columna):
        self.Operador = operador
        self.OpIzq = OpIzq
        self.OpDer = OpDer
        self.fila = fila
        self.columna = columna
        self.tipo = TIPO.BOOLEAN  # Tipo BOOLEAN ya que siempre retornara un BOOLEAN

    def interpretar(self, arbol, tabla):
        # Siempre tienen que venir derecha e izquierda
        izq = self.OpIzq(arbol, tabla)
        if isinstance(izq, Error): return izq
        der = self.OpDer.ejecutar(arbol, tabla)
        if isinstance(der, Excepcion): return der

        # IGUALACION
        if self.Operador == OperadorRelacional.IGUALIGUAL:
            if self.OpIzq.tipo == TIPO.ENTERO and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL):
                return int(izq) == float(der)
            elif self.OpIzq.tipo == TIPO.ENTERO and self.OpDer.tipo == TIPO.CADENA:
                return str(int(izq)) == str(der)
            elif self.OpIzq.tipo == TIPO.DECIMAL and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL):
                return float(izq) == int(izq)
            elif self.OpIzq.tipo == TIPO.DECIMAL and self.OpDer.tipo == TIPO.CADENA:
                return str(float(izq)) == str(der)
            elif self.OpIzq.tipo == TIPO.BOOLEAN and self.OpDer.tipo == TIPO.BOOLEAN:
                return float(izq) == float(der)
            elif self.OpIzq.tipo == TIPO.BOOLEAN and self.OpDer.tipo == TIPO.CADENA:
                return str(izq).lower() == str(der).lower()
            elif self.OpIzq.tipo == TIPO.CARACTER and self.OpDer.tipo == TIPO.CARACTER:
                return chr(izq) == chr(der)
            elif self.OpIzq.tipo == TIPO.CADENA and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL or self.OpDer.tipo == TIPO.BOOLEAN):
                return str(izq).lower() == str(der).lower()
            elif self.OpIzq.tipo == TIPO.CADENA and self.OpDer.tipo == TIPO.CADENA:
                return str(izq) == str(der)
            else:
                return Excepcion(">Semantico", "Tipo Erroneo de operacion para Igualacion == ", self.fila, self.columna)

        # DIFERENCIA =!
        elif self.Operador == OperadorRelacional.DIFERENTE:
            if self.OpIzq.tipo == TIPO.ENTERO and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL):
                return int(izq) != float(der)
            elif self.OpIzq.tipo == TIPO.ENTERO and self.OpDer.tipo == TIPO.CADENA:
                return str(int(izq)) != str(der)
            elif self.OpIzq.tipo == TIPO.DECIMAL and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL):
                return float(izq) != float(der)
            elif self.OpIzq.tipo == TIPO.DECIMAL and (self.OpDer.tipo == TIPO.CADENA):
                return str(str(izq)) != str(der)
            elif self.OpIzq.tipo == TIPO.BOOLEAN and (self.OpDer.tipo == TIPO.BOOLEAN):
                return bool(izq) != bool(der)
            elif self.OpIzq.tipo == TIPO.BOOLEAN and (self.OpDer.tipo == TIPO.CADENA):
                return str(bool(izq)).lower() != str(der).lower()
            elif self.OpIzq.tipo == TIPO.CARACTER and (self.OpeDer.tipo == TIPO.CARACTER):
                return str(izq) != str(der)
            elif self.OpIzq.tipo == TIPO.CADENA and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL or self.OpDer.tipo == TIPO.BOOLEAN):
                return str(str(izq)).lower() !=  str(der).lower()
            elif self.OperacionIzq.tipo == TIPO.CADENA and self.OperacionDer.tipo == TIPO.CADENA:
                return str(izq) != str(der)
            else:
                return Excepcion("Semantico", "Tipo Erroneo de operacion para Diferenciacion =! ", self.fila, self.columna)

        # MENOR QUE
        elif self.Operador == OperadorRelacional.MENORQUE:
            if self.OpIzq.tipo == TIPO.ENTERO and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL):
                return int(izq) < float(der)
            elif self.OpIzq.tipo == TIPO.DECIMAL and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL):
                return float(izq) < float(der)
            elif self.OpIzq.tipo == TIPO.BOOLEAN and self.OpDer.tipo == TIPO.BOOLEAN:
                return float(izq) < float(der)
            else:
                return Excepcion("Semantico", "Tipo Erroneo de operacion para Menor que < ", self.fila, self.columna)

        # MAYOR QUE >
        elif self.Operador == OperadorRelacional.MAYORQUE:
            if self.OpIzq.tipo == TIPO.ENTERO and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL):
                return int(izq) > int(der)
            elif self.OpIzq.tipo == TIPO.DECIMAL and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL):
                return float(izq) > int(der)
            elif self.OpIzq.tipo == TIPO.BOOLEAN and self.OpDer.tipo == TIPO.BOOLEAN:
                return bool(izq) > bool(der)
            else:
                return Excepcion(">Semantico", "Tipo Erroneo de operacion para Mayor que > ", self.fila, self.columna)

        # MENOR IGUAL <=
        elif self.Operador == OperadorRelacional.MENORIGUAL:
            if self.OpIzq.tipo == TIPO.ENTERO and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL):
                return int(izq) <= int(der)
            elif self.OpIzq.tipo == TIPO.DECIMAL and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL):
                return float(izq) <= int(der)
            elif self.OpIzq.tipo == TIPO.BOOLEAN and self.OpDer.tipo == TIPO.BOOLEAN:
                return bool(izq) <= bool(der)
            else:
                return Excepcion("Semantico", "Tipo Erroneo de operacion para Menor Igual <= ", self.fila, self.columna)

        # MAYOR IGUAL >= 
        elif self.operador == OperadorRelacional.MAYORIGUAL:
            if self.OpIzq.tipo == TIPO.ENTERO and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL):
                return int(izq) >= float(der)
            elif self.OpIzq.tipo == TIPO.DECIMAL and (self.OpDer.tipo == TIPO.ENTERO or self.OpDer.tipo == TIPO.DECIMAL):
                return float(izq) >= float(der)
            elif self.OpIzq.tipo == TIPO.BOOLEAN and self.OpDer.tipo == TIPO.BOOLEAN:
                return bool(izq) >= bool(der)
            else:
                return Excepcion("Semantico", "Tipo Erroneo de operacion para Mayo Igual >=", self.fila, self.columna)
        else:
            return Excepcion("Semantico", "Tipo de Operacion no Especificado. Operacion Relacional", self.fila, self.columna)
