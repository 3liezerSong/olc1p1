from Ambito.Arbol import NodoArbol
from Ambito.Instrucciones import Instrucciones
from Tablas_simbolos.Error import Error

class Identificador(Instrucciones): #{identificador,fila,columna}
    def __init__(self, _identificador, _fila, _columna):
        self.identificador = _identificador
        self.tipo = None
        self.fila = _fila
        self.columna = _columna

    def ejecutar(self, arbol, tabla):
        simbolo = tabla.getTabla(self.identificador.lower())
        if simbolo is None:
            return Error("Semantico", "Variable " + self.identificador + " no encontrada", self.fila, self.columna)
        self.tipo = simbolo.getTipo()#ahora tenemos el tipo del identificador dentro de la tabla
        return simbolo.getValor()
