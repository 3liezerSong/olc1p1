from Ambito.Instrucciones import Instrucciones
from Ambito.Arbol import Ambito
from Tablas_simbolos.Error import Error
from Enums.TipoOperacion import OperadorLogico
from Enums.TipoValor import TIPO

class Logica(Instrucciones):
    def __init__(self, _operador, _OpIzq, _OpDer, _fila, _columna):
        self.tipo = TIPO.BOOLEAN
        self.Operador = _operador
        self.OpIzq = _OpIzq 
        self.OpDer = _OpDer
        self.fila = _fila
        self.columna = _columna

    def ejecutar(self, arbol, tabla):
        izq = self.OpIzq.ejecutar(arbol,tabla)
        if isinstance(izq,Error):
            return izq
        if self.OpDer is not None:
            der = self.OpDer.ejecutar(arbol,tabla)
            if isinstance(der,Error):
                return der
# PARA OR
        if self.Operador == OperadorLogico.OR and self.OpIzq.tipo == self.OpDer.tipo and self.OpIzq.tipo == TIPO.BOOLEAN:
            return bool(izq) or bool(der)
        elif self.Operador == OperadorLogico.OR:
            return Error("Semantico", "ERROR en la operacion OR", self.fila, self.columna)
# PARA AND
        if self.Operador == OperadorLogico.AND and self.OpIzq.tipo == self.OpDer.tipo and self.OpDer.tipo == TIPO.BOOLEAN:
            return bool(izq) and  bool(der)
        elif self.Operador == OperadorLogico.AND:
            return Error("Semantico", "ERROR en la operacion && ", self.fila, self.columna)
# PARA NOT
        if self.Operador == OperadorLogico.NOT and self.OpIzq.tipo == TIPO.BOOLEAN:
                return bool(izq)
        elif self.Operador == OperadorLogico.NOT:
                return Error("Semantico", "ERROR en la operacion Not", self.fila, self.columna)
        else:
            pass
        
        return Error("Semantico","Tipo de operacion Logica no especificada", self.fila, self.columna)
