%{
    var listErrores = [];
    var Tokens = [];
    /*Agrego los "errores" a una lista*/
    function AgregarError(tipo,value,descripcion,fila,columna){
		newError = new NodoError(tipo,value,descripcion,fila,columna);
		listErrores.push(newError);
	}
    
    /*Agrego los "tokens" a una lista*/
	function AgregarToken(line,columna,tipo,descripcion){
		nuevoToken = new TokenList(line,columna,tipo,descripcion);
		Tokens.push(nuevoToken);
	}
%}

/*Definición Léxica*/
%lex
/*Donde A=a y Ac = ac*/
%options case-insensitive

%%

\s+                                  {} /*espacion en blanco*/
"//".*  {}                              /*comentarios unilinea*/
[/][*][^*]*[*]+([^/*][^*]*[*]+)*[/]  {} /*comentarios multilinea*/
[ \r\t]+			                 {}
\n				                     {}

/*tipodatos*/
"int"     return 'Int_'
"double"  return 'Double_'
"boolean" return 'Boolean_'
"char"    return 'Char_'
"string"  return 'String_'

"true"    return 'TRUE_'
"false"  return 'FALSE_'

/*sentencias*/
"void" return 'Void_'
"exec" return 'Exec_'
"print"   return 'Print_'
"if"      return 'If_'
"else"    return 'Else_'
"switch"  return 'Switch_'
"case"    return 'Case_'
"default" return 'Default_'
"while"   return 'While_'
"for"     return 'For_'
"do"      return 'Do_'
"break"   return 'Break_'
"continue" return 'Continue_'
"return"   return 'Return_'
"tolower"  return 'ToLower_'
"toupper"  return 'ToUpper_'
"length"   return 'Length_'
"truncate" return 'Truncate_'
"round"    return 'Round_'
"typeof"   return 'Typeof_'
"tostring" return 'ToString_'
"tochararray" return 'ToCharArray_'

/*agrupacion*/
"(" return 'ParA'
")" return 'ParC'
"{" return 'LLaA'
"}" return 'LLaC'
"." return 'Punto'
"," return 'Coma'
":" return 'DosPuntos'
";" return 'Ptcoma'
"[" return 'CorAbre'
"]" return 'CorCierre'
"new" return 'New_'
"list" return 'List_'

/*aritmeticos*/
"+" return 'Suma'
"--" return 'Menos' /*sin precedencia*/
"-" return 'RESTA'
"++" return 'Mas' /*sin precedencia*/
"*" return 'Multiplicacion'
"^" return 'Potencia'
"/" return 'Division'
"%" return 'Modulo'


/*relacionales*/
"<=" return 'Menorigual'
">=" return 'Mayorigual'
"==" return 'Igualigual'
"!=" return 'Diferente'
"="  return 'Igual' /*sin precedencia*/
"<"  return 'Menor'
">"  return 'Mayor'
"?"  return 'Interrogar'

/*logicos*/
"!"  return 'Not'
"&&" return 'And'
"||" return 'Or'


/*strings o chars*/
"'"[^']"'" return 'Caracter'
\"[^\"]*\" return 'Cadena_'
[a][d][d] return 'Add_' /*add a la lista*/
[0-9]+[.][0-9]+ return 'Decimal'   /*decimales [0-9]+("."[0-9]+)?   enteros*/
[0-9]+    return 'Numero'



([a-zA-Z_])[a-zA-Z0-9_]*  return 'Identificador'  /*identificadores*/


<<EOF>>               return 'EOF'
.                     return 'INVALID'


/lex
%{
	const TIPO_OPERACION	= require('./controlador/Enums/TipoOperacion');
	const TIPO_VALOR 		= require('./controlador/Enums/TipoValor');
	const TIPO_DATO			= require('./controlador/Enums/TipoDato'); //para jalar el tipo de d
  const instruccionesAPI	= require('./controlador/Instruccion/Instruccion');
  const NodoError	= require('./controlador/Instruccion/Error');
%}

/* Asociación de Operadores y Precedencia */
%left 'Or'
%left 'And'
%right 'Not'
%left 'Igualigual' 'Diferente'
%left 'Menorigual' 'Mayorigual' 'Menor' 'Mayor'
%left 'Suma' 'RESTA'
%left 'Multiplicacion' 
'Division' 'Modulo' 
%left 'Potencia'
%left 'Mas' 'Menos'

%left URESTA
%right UNOT


%start INICIO

%% /* Definición de la gramática */

INICIO: CUERPOTOTAL EOF {console.log(JSON.stringify($1,null,2));console.log(listErrores); return $1;}
;

CUERPOTOTAL: 
            CUERPOTOTAL CUERPO {$1.push($2); $$=$1;}
          | CUERPO {$$ = [$1];};

CUERPO: 
       DECLARACION {$$ = $1}
    |  ASIGNACION {$$ = $1}
    |  DEC_FUN {$$ = $1}
    |  Void_ Identificador ParA LISTAPARAMETRO ParC CUERPOSENTENCIA  {$$ = instruccionesAPI.nuevoVoid($2,$1,$4,$6,this._$.first_line,this._$.first_column+1);}
    |  Void_ Identificador ParA ParC CUERPOSENTENCIA  {$$ = instruccionesAPI.nuevoVoid($2,$1,null,$5,this._$.first_line,this._$.first_column+1);}
    |  Exec_ Identificador ParA EXPRESIONES ParC Ptcoma {$$ = instruccionesAPI.nuevoExec($2,$4,this._$.first_line,this._$.first_column+1);}
    |  Exec_ Identificador ParA ParC Ptcoma {$$ = instruccionesAPI.nuevoExec($2,null,this._$.first_line,this._$.first_column+1);}
;

EXPRESIONES: EXPRESIONES Coma EXPRESION {$1.push($3); $$=$1;}
    | EXPRESION {$$ = [$1];}
;

DECLARACION:
      TIPODATO LISTAIDENTIFICADORES Igual EXPRESION Ptcoma  {$$ = instruccionesAPI.nuevoDeclaracion($2, $4,$1,this._$.first_line,this._$.first_column+1);}
    | TIPODATO LISTAIDENTIFICADORES Ptcoma {$$ = instruccionesAPI.nuevoDeclaracion($2,null, $1,this._$.first_line,this._$.first_column+1);}
    /*Vectores*/
    | TIPODATO CorAbre CorCierre LISTAIDENTIFICADORES Igual New_ TIPODATO CorAbre EXPRESION CorCierre Ptcoma {$$ = instruccionesAPI.nuevoDeclaracionVector($4, $9,$1,this._$.first_line,this._$.first_column+1);}
    | TIPODATO CorAbre CorCierre LISTAIDENTIFICADORES Igual LLaA EXPRESIONES LLaC Ptcoma {$$ = instruccionesAPI.nuevoDeclaracionVectorTipo2($4, $7,$1,this._$.first_line,this._$.first_column+1);}
    /* Listas*/
    | List_  Menor TIPODATO Mayor Identificador Igual New_ List_ Menor TIPODATO Mayor Ptcoma {$$= instruccionesAPI.nuevoDeclaracionLista($5,null,$3,this._$.first_line,this._$.first_column+1);}
    | List_  Menor TIPODATO Mayor Identificador Igual FUN_PRIMITIVA Ptcoma {$$ = instruccionesAPI.nuevoDeclaracionToCharArray($5,$7,$3,this._$.first_line,this._$.first_column+1);} 
    |  error {AgregarError("Sintactico",yytext,"Instruccion No Definida",this._$.first_line,this._$.first_column);}

;

ASIGNACION:
        Identificador Igual EXPRESION Ptcoma {$$ = instruccionesAPI.nuevoAsignacion($1,$3,this._$.first_line,this._$.first_column+1);}
      | Identificador Igual EXPRESION Interrogar PRIMITIVO DosPuntos PRIMITIVO Ptcoma     {$$= instruccionesAPI.nuevoTernario($1,$3,$5,$7,this._$.first_line,this._$.first_column+1);}
      | Identificador Igual EXPRESION Interrogar FUN_PRIMITIVA DosPuntos FUN_PRIMITIVA Ptcoma {$$= instruccionesAPI.nuevoTernario($1,$3,$5,$7,this._$.first_line,this._$.first_column+1);}
        /*Listas*/
      | Identificador CorAbre CorAbre EXPRESION CorCierre CorCierre Igual EXPRESION Ptcoma {$$ = instruccionesAPI.nuevoAsignacionLista2($1,$4,$8,this._$.first_line,this._$.first_column+1);}
      | Identificador Punto Add_ ParA EXPRESION ParC Ptcoma {$$ = instruccionesAPI.nuevoAsignacionLista($1,$5,this._$.first_line,this._$.first_column+1);}
        /*Vecores*/
      | Identificador CorAbre EXPRESION CorCierre Igual EXPRESION Ptcoma {$$ = instruccionesAPI.nuevoAsignacionVector($1,$3,$6,this._$.first_line,this._$.first_column+1);}
        /*incremento y decremento*/
      | Identificador Suma Suma   Ptcoma {$$ = instruccionesAPI.nuevoIncremento($1,null,this._$.first_line,this._$.first_column+1);}
      | Identificador Menos Ptcoma {$$ = instruccionesAPI.nuevoDecremento($1,null,this._$.first_line,this._$.first_column+1);}

;

DEC_FUN:
      TIPODATO Identificador ParA LISTAPARAMETRO ParC CUERPOSENTENCIA {$$ = instruccionesAPI.nuevoFuncion($2,$1,$4,$6,this._$.first_line,this._$.first_column+1);}
    | TIPODATO Identificador ParA ParC CUERPOSENTENCIA {$$ = instruccionesAPI.nuevoFuncion($2,$1,null,$5,this._$.first_line,this._$.first_column+1);}
;

LISTAPARAMETRO:
      LISTAPARAMETRO Coma PARAMETRO {$1.push($3); $$=$1;}
    | PARAMETRO                     {$$ = [$1];}
;

PARAMETRO:
      TIPODATO Identificador  {$$ = instruccionesAPI.nuevoDeclaracion($2,null, $1,this._$.first_line,this._$.first_column+1);}
;  

EXPRESION:
        /*Aritmetica*/
      EXPRESION Suma EXPRESION       {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.SUMA,this._$.first_line,this._$.first_column+1);}
    | EXPRESION RESTA EXPRESION      {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.RESTA,this._$.first_line,this._$.first_column+1);}
    | EXPRESION Multiplicacion EXPRESION {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.MULTIPLICACION,this._$.first_line,this._$.first_column+1);}
    | EXPRESION Division EXPRESION   {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.DIVISION,this._$.first_line,this._$.first_column+1);}
    | EXPRESION Modulo EXPRESION     {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.MODULO,this._$.first_line,this._$.first_column+1);}
    | EXPRESION Potencia EXPRESION   {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.POTENCIA,this._$.first_line,this._$.first_column+1);}
        /*Relacional*/
    | EXPRESION Menorigual EXPRESION {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.MENOR_IGUAL,this._$.first_line,this._$.first_column+1);}
    | EXPRESION Mayorigual EXPRESION {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.MAYOR_IGUAL,this._$.first_line,this._$.first_column+1);}
    | EXPRESION Igualigual EXPRESION {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.DOBLE_IGUAL,this._$.first_line,this._$.first_column+1);}
    | EXPRESION Diferente EXPRESION  {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.NO_IGUAL,this._$.first_line,this._$.first_column+1);}
    | EXPRESION Menor EXPRESION      {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.MENOR_QUE,this._$.first_line,this._$.first_column+1);}
    | EXPRESION Mayor EXPRESION      {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.MAYOR_QUE,this._$.first_line,this._$.first_column+1);}
        /*Logicos*/
    | EXPRESION And EXPRESION        {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.AND,this._$.first_line,this._$.first_column+1);}
    | EXPRESION Or  EXPRESION        {$$ = instruccionesAPI.nuevoOperacionBinaria($1, $3, TIPO_OPERACION.OR,this._$.first_line,this._$.first_column+1);}
    | Not EXPRESION                  {$$ = instruccionesAPI.nuevoOperacionBinaria($2,null, TIPO_OPERACION.NOT,this._$.first_line,this._$.first_column+1);}
    | ParA EXPRESION ParC            {$$ = $2}
    | RESTA EXPRESION %prec URESTA   {$$ = instruccionesAPI.nuevoOperacionBinaria($2,null, TIPO_OPERACION.NEGATIVO,this._$.first_line,this._$.first_column+1);}*/
    /*| PRIMITIVO Mas              {$$ = instruccionesAPI.nuevoOperacionBinaria($1,"1",TIPO_OPERACION.SUMA,this._$.first_line,this._$.first_column+1);}*/
    /*| PRIMITIVO Menos            {$$ = instruccionesAPI.nuevoOperacionBinaria($1,"1",TIPO_OPERACION.URESTA,this._$.first_line,this._$.first_column+1);}*/
    | PRIMITIVO                      {$$ = $1}
    | FUN_PRIMITIVA                  {$$ = $1}
    | PRIMITIVO_VECTOR               {$$ = $1}
    | PRIMITIVO_LISTA                {$$ = $1}
    | CASTEO                         {$$ = $1}
    | Identificador ParA EXPRESIONES ParC{$$ = instruccionesAPI.valReturn($1,$3,TIPO_OPERACION.VAL_RETURN, this._$.first_line,this._$.first_column+1);}
    | Identificador  ParA ParC {$$ = instruccionesAPI.valReturn($1,$3,TIPO_OPERACION.VAL_RETURN, this._$.first_line,this._$.first_column+1);}
    /*| EXPRESION Interrogar EXPRESION EXPRESION {$$ = instruccionesAPI.nuevoTernario($1,$3,$4,this._$.first_line,this._$.first_column+1);}*/
;
PRIMITIVO:
      Cadena_        {$$ = instruccionesAPI.nuevoValor($1, TIPO_VALOR.CADENA,this._$.first_line,this._$.first_column+1);}
    | Caracter       {$$ = instruccionesAPI.nuevoValor($1, TIPO_VALOR.CARACTER,this._$.first_line,this._$.first_column+1);}
    | Decimal        {$$ = instruccionesAPI.nuevoValor($1, TIPO_VALOR.DECIMAL,this._$.first_line,this._$.first_column+1);}
    | Numero         {$$ = instruccionesAPI.nuevoValor($1, TIPO_VALOR.NUMERO,this._$.first_line,this._$.first_column+1);}
    | TRUE_          {$$ = instruccionesAPI.nuevoValor($1, TIPO_VALOR.BOOLEAN,this._$.first_line,this._$.first_column+1);}
    | FALSE_         {$$ = instruccionesAPI.nuevoValor($1, TIPO_VALOR.BOOLEAN,this._$.first_line,this._$.first_column+1);}
    | Identificador  {$$ = instruccionesAPI.nuevoValor($1, TIPO_VALOR.IDENTIFICADOR,this._$.first_line,this._$.first_column+1);}
;
FUN_PRIMITIVA:
      ToLower_ ParA EXPRESION ParC  {$$= instruccionesAPI.nuevoOperacionNativa($3,null,TIPO_OPERACION.TO_LOWER,this._$.first_line,this._$.first_column+1);}
    | ToUpper_ ParA EXPRESION ParC  {$$= instruccionesAPI.nuevoOperacionNativa($3,null,TIPO_OPERACION.TO_UPPER,this._$.first_line,this._$.first_column+1);}
    | ToString_ ParA EXPRESION ParC {$$= instruccionesAPI.nuevoOperacionNativa($3,null,TIPO_OPERACION.TO_STRING,this._$.first_line,this._$.first_column+1);}
    | Truncate_ ParA EXPRESION ParC {$$= instruccionesAPI.nuevoOperacionNativa($3,null,TIPO_OPERACION.TRUNCATE,this._$.first_line,this._$.first_column+1);}
    | Round_ ParA EXPRESION ParC    {$$= instruccionesAPI.nuevoOperacionNativa($3,null,TIPO_OPERACION.ROUND,this._$.first_line,this._$.first_column+1);}
    | Typeof_ ParA EXPRESION ParC   {$$= instruccionesAPI.nuevoOperacionNativa($3,null,TIPO_OPERACION.TYPEOF,this._$.first_line,this._$.first_column+1);}
    | Length_ ParA EXPRESION ParC   {$$= instruccionesAPI.nuevoOperacionNativa($3,null,TIPO_OPERACION.LENGTH,this._$.first_line,this._$.first_column+1);}
    | ToCharArray_ ParA EXPRESION ParC {$$= instruccionesAPI.nuevoOperacionNativa($3,null,TIPO_OPERACION.TO_STRING,this._$.first_line,this._$.first_column+1);}
;
CASTEO:
      ParA TIPODATO ParC PRIMITIVO {$$= instruccionesAPI.nuevoCasteo($2,$4,TIPO_OPERACION.CASTEO,this._$.first_line,this._$.first_column+1);}
    | ParA TIPODATO ParC FUN_PRIMITIVA {$$= instruccionesAPI.nuevoCasteo($2,$4,TIPO_OPERACION.CASTEO,this._$.first_line,this._$.first_column+1);}
;

PRIMITIVO_VECTOR:
     PRIMITIVO CorAbre EXPRESION CorCierre {$$= instruccionesAPI.nuevoAccederVector($1,$3,TIPO_OPERACION.ACCESO_VECTOR,this._$.first_line,this._$.first_column+1);} 
     
;
PRIMITIVO_LISTA:
     PRIMITIVO CorAbre CorAbre EXPRESION CorCierre CorCierre {$$ = instruccionesAPI.nuevoAccederLista($1,$4,TIPO_OPERACION.ACCESO_LISTA,this._$.first_line,this._$.first_column+1);}
;

TERNARIO:
     EXPRESION Interrogar EXPRESION EXPRESION {$$ = instruccionesAPI.nuevoTernario($1,$3,$4,this._$.first_line,this._$.first_column+1);}
;

CUERPOSENTENCIA:
      LLaA VARIASSENTENCIAS LLaC {$$ = $2}
    | LLaA LLaC                  {}
;

VARIASSENTENCIAS:
      VARIASSENTENCIAS SENTENCIAS {$1.push($2); $$=$1;}
    | SENTENCIAS                  {$$ = [$1];}
;

SENTENCIAS:
      /*Declarativas*/
      DECLARACION {$$ = $1}
    | ASIGNACION {$$ = $1}
      /*de control*/
    | IFF       {$$ = $1}
    | SWITCHH   {$$ = $1}
      /*de repetición(cíclicas)*/
    | WHILEE    {$$ = $1}
    | FORR      {$$ = $1}
    | DOWHILEE  {$$ = $1}
      /*de transferencia*/
    | BREAKK    {$$ = $1}
    | CONTINUEE {$$ = $1}
    | RETURNN   {$$ = $1}
      /*LLAMADAS*/
    | LLAMADAA  {$$ = $1} /*invocamos metodos*/

    | PRINTT    {$$ = $1}
;

TIPODATO:
      Int_     {$$ = TIPO_DATO.INT}
    | Double_  {$$ = TIPO_DATO.DOUBLE}
    | Boolean_ {$$ = TIPO_DATO.BOOLEAN}
    | Char_    {$$ = TIPO_DATO.CHAR}
    | String_  {$$ = TIPO_DATO.STRING}
;

LISTAIDENTIFICADORES:
      LISTAIDENTIFICADORES Coma ID {$1.push($3); $$=$1;}
    | ID {$$ = [$1];}
;

ID: 
      Identificador {$$ = instruccionesAPI.nuevoValor($1,TIPO_VALOR.IDENTIFICADOR,this._$.first_line,this._$.first_column+1);}
;

IFF:
      If_ CONDICION CUERPOSENTENCIA    {$$ = instruccionesAPI.nuevoIf($2, $3,this._$.first_line,this._$.first_column+1);}
    | If_ CONDICION CUERPOSENTENCIA Else_ CUERPOSENTENCIA {$$ = instruccionesAPI.nuevoIfElse($2,$3,$5,this._$.first_line,this._$.first_column+1);}
    | If_ CONDICION CUERPOSENTENCIA ELSEIF {$$ = instruccionesAPI.nuevoIfElseIf($2,$3,$4,null,this._$.first_line,this._$.first_column+1);}
    | If_ CONDICION CUERPOSENTENCIA ELSEIF Else_ CUERPOSENTENCIA {$$ = instruccionesAPI.nuevoIfElseIf($2,$3,$4,$6,this._$.first_line,this._$.first_column+1);}
    /*duda con este FALTAN ALGUNOS CASOS*/
;

ELSEIF: ELSEIF CIF{$1.push($2); $$ = $1;}
      | CIF {$$=[$1];}
;

CIF: Else_ If_ CONDICION CUERPOSENTENCIA {$$ = instruccionesAPI.nuevoElseIf($3,$4,this._$.first_line,this._$.first_column+1);}
;

CONDICION:
     ParA EXPRESION ParC {$$=$2}
;

WHILEE:
     While_ CONDICION CUERPOSENTENCIA {$$ = instruccionesAPI.nuevoWhile($2,$3,this._$.first_line,this._$.first_column+1);}
;

DOWHILEE:
     Do_ CUERPOSENTENCIA While_ CONDICION Ptcoma {$$ = instruccionesAPI.nuevoDoWhile($2,$4,this._$.first_line,this._$.first_column+1);}
;

FORR:
      For_ ParA ASIGNACION EXPRESION Ptcoma PASO ParC CUERPOSENTENCIA {$$= instruccionesAPI.nuevoFor($3,$4,$6,$8,this._$.first_line,this._$.first_column+1);}
    | For_ ParA DECLARACION EXPRESION Ptcoma PASO ParC CUERPOSENTENCIA {$$=instruccionesAPI.nuevoFor($3,$4,$6,$8,this._$.first_line,this._$.first_column+1);}
;

PASO:
      Identificador Igual EXPRESION {$$ = instruccionesAPI.nuevoAsignacion($1,$3,this._$.first_line,this._$.first_column+1);}
    | Identificador Suma Suma {$$ = instruccionesAPI.nuevoIncremento($1,null,this._$.first_line,this._$.first_column+1);}
    | Identificador Menos {$$ = instruccionesAPI.nuevoDecremento($1,null,this._$.first_line,this._$.first_column+1);}
;

SWITCHH:
    Switch_ ParA EXPRESION ParC LLaA CASOS LLaC {$$ = instruccionesAPI.nuevoSwich($3,$6,this._$.first_line,this._$.first_column+1);} 
;

CASOS:
      CASOS CASOS_EVALUAR {$1.push($2); $$=$1;}
    | CASOS_EVALUAR       {$$ = [$1];}
;

CASOS_EVALUAR:
      Case_ EXPRESION DosPuntos VARIASSENTENCIAS    {$$ = instruccionesAPI.nuevoCaso($1,$3,this._$.first_line,this._$.first_column+1);}
    | Default_ DosPuntos VARIASSENTENCIAS {$$ = instruccionesAPI.nuevoDefault($3,this._$.first_line,this._$.first_column+1)}
;
LLAMADAA:
      Identificador ParA EXPRESIONES ParC Ptcoma {$$ = instruccionesAPI.nuevoLlamada($1,null, $3, this._$.first_line,this._$.first_column+1);}
    | Identificador  ParA ParC Ptcoma {$$ = instruccionesAPI.nuevoLlamada($1,null,null, this._$.first_line,this._$.first_column+1);}
;
PRINTT:
     Print_ CONDICION Ptcoma {$$ = instruccionesAPI.nuevoPrint($2,this._$.first_line,this._$.first_column+1);}
;
BREAKK:
    Break_ Ptcoma {$$ = instruccionesAPI.nuevoBreak(this._$.first_line,this._$.first_column+1);}
;
CONTINUEE:
    Continue_ Ptcoma {$$ = instruccionesAPI.nuevoContinue(this._$.first_line,this._$.first_column+1);}
;
RETURNN:
    Return_ EXPRESION Ptcoma {$$ = instruccionesAPI.nuevoReturn($2,this._$.first_line,this._$.first_column+1);}
;