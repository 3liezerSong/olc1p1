from enum import Enum

class OperadorAritmetico(Enum):
    MAS = 1
    RESTA = 2
    POTENCIA = 3
    MULTIPLICAR = 4
    DIVIDIR = 5
    MODULO = 6
    UMENOS = 7
    MASMAS = 8
    MENOSMENOS = 9

class OperadorRelacional(Enum):
    MENORQUE = 1
    MAYORQUE = 2
    MENORIGUAL = 3
    MAYORIGUAL = 4
    IGUALIGUAL = 5
    DIFERENTE = 6

class OperadorLogico(Enum):
    NOT = 1
    AND = 2
    OR = 3

