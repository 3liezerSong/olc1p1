from enum import Enum

class TIPO(Enum):
    ENTERO = 1
    DECIMAL = 2
    BOOLEAN = 3
    CARACTER = 4
    CADENA = 5
    NULO = 6
    ARREGLO = 7