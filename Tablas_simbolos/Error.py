class Error:
    def __init__(self, _tipo, _descripcion, _fila, _columna): #{tipo,descripcion,fila,columna}
        self.tipo = _tipo
        self.descripcion = _descripcion
        self.fila = _fila
        self.columna = _columna

    def getTipo(self):
        return self.tipo
    def setTipo(self, tipo):
        self.tipo = tipo
    def getDescripcion(self):
        return self.descripcion
    def setDescripcion(self, descripcion):
        self.descripcion = descripcion
    def getFila(self):
        return self.fila
    def setFila(self, fila):
        self.fila = fila
    def getColumna(self):
        return self.columna
    def setColumna(self, columna):
        self.columna = columna
    def toString(self):
        return self.tipo + " - " + self.descripcion + " [" + str(self.fila) + "," + str(self.columna) + "]"
