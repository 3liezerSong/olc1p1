# LISTO
from Ambito.Instrucciones import Instrucciones
from  Ambito.Arbol import NodoArbol

class Primitivos(Instrucciones): # {tipo,valor,fila,columna}
    def __init__(self, _tipo, _valor, _fila, _columna):
        self.tipo = _tipo
        self.valor = _valor
        self.fila = _fila
        self.columna = _columna

    def ejecutar(self, arbol, tabla):
        return self.valor