#
from abc import ABC, abstractmethod

class Instrucciones(ABC):
    def __init__(self, fila, columna):
        self.fila = fila
        self.columna = columna
        super().__init__()

    @abstractmethod
    def ejecutar(self, arbol, tabla):
        pass

    @abstractmethod
    def getNodo(self):
        pass