#
class Simbolo:
    def __init__(self, _id, _tipo, _fila, _columna, _valor):
        self.id = _id
        self.valor = _valor
        self.tipo = _tipo
        self.fila = _fila
        self.columna = _columna

    def setID(self, id):
        self.id = id
    def getID(self):
        return self.id
    def setValor(self, valor):
        self.valor = valor
    def getValor(self):
        return self.valor
    def setTipo(self, tipo):
        self.tipo = tipo
    def getTipo(self):
        return self.tipo
    def getFila(self):
        return self.fila
    def getColumna(self):
        return self.columna