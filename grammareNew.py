import os.path
import sys

reserved = {
    'cond'    : 'econd',
    'instr'   : 'einstr',
    'imprimir': 'eimprimir',

    'false':    'efalse',
    'true':     'etrue',
    'null':     'enull',

    'int':      'eint',
    'double':   'edouble',
    'boolean':  'eboolean',
    'char':     'echar',
    'string':   'estring',

    'while' : 'ewhile',
    'do'    : 'edo',
    'for'   : 'efor',
    'if'    : 'eif',
    'else'  : 'eelse',
}


errors = []
console = ""

tokens = [
    'ecomentarios',
    'ecomentario',    
    'edspuntos',      

    'esumar',        
    'eresta',
    'emultiplicar',  
    'edivision',     
    'epotencia',       
    'emodulo',      

    'eigualigual',  
    'ediferente',  
    'emenor',      
    'emayor',     
    'emenorigual',       
    'emayorigual', 

    'eor', 
    'eand',
    'not',

    'eparabre', 
    'eparcierra',
    'ecoma',

    'eptcoma', 
    'ellaabre', 
    'ellacierra',

    'eigual',

    'eincremento',        
    'edecremento', 

    'ecorabre',  
    'ecorcierra',

    'eentero', 
    'edecimal', 
    'ecaracter', 
    'ecadena',
    'id',


] + list(reserved.values())

from Tabla_Simbolo.TablaSimbolos import lista_variables
from Ambito.NodoArbol import NodoArbol
# Tokens

t_epotencia = r'\*\*'
t_edivision = r'/'
t_emultiplicar = r'\*'
t_emodulo = r'\%'
t_esumar = r'\+'
t_eresta = r'\-'

t_edspuntos = r':'
t_eptcoma = r';'
t_ecoma = r','

t_eigualigual = r'=='
t_eigual = r'='
t_ediferente = r'!='
t_emenor = r'<'
t_emayor = r'>'
t_emenorigual = r'<='
t_emayorigual = r'>='

t_eor = r'\|\|'
t_eand = r'&&'
t_not = r'!'

t_eparabre = r'\('
t_eparcierra = r'\)'
t_ecorabre = r'\['
t_ecorcierra = r'\]'
t_ellaabre = r'\{'
t_ellacierra = r'\}'

t_eincremento = '\+\+'
t_edecremento = '\-\-'

def t_edecimal(t):
    r'\d+\.\d+'
    try:
        t.value = float(t.value)
    except ValueError:
        print("Float Value too large %d", t.value)
        t.value = 0
    return t

def t_eentero(t):
    r'\d+'
    try:
        t.value = int(t.value)
    except ValueError:
        print("Integer value too large %d", t.value)
        t.value = 0
    return t

def t_id(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value.lower(), 'id')
    return t


def t_ecadena(t):
    r"""\"(\\"|\\'|\\\\|\\n|\\t|\\r|[^\\\'\"])*?\""""
    t.value = t.value[1:-1] 
    t.value = t.value.replace('\\n', '\n')
    t.value = t.value.replace('\\r', '\r')
    t.value = t.value.replace('\\\\', '\\')
    t.value = t.value.replace('\\"', '\"')
    t.value = t.value.replace('\\t', '\t')
    t.value = t.value.replace("\\'", '\'')
    return t

def t_ecaracter(t):
    r""" \'(\\'|\\\\|\\n|\\t|\\r|\\"|.)?\'"""
    t.value = t.value[1:-1]
    t.value = t.value.replace('\\n', '\n')
    t.value = t.value.replace('\\r', '\r')
    t.value = t.value.replace('\\\\', '\\')
    t.value = t.value.replace('\\"', '\"')
    t.value = t.value.replace('\\t', '\t')
    t.value = t.value.replace("\\'", '\'')
    return t


def t_ecomentario(t):
    r'\#\&(.|\n)*?\&\#'
    t.lexer.lineno += t.value.count('\n')

def t_ecomentarios(t):
    r'\#.*\n'
    t.lexer.lineno += 1

# Caracters ignorados
t_ignore = " \t\r"

def t_enuevalinea(t):
    r'\n+'
    t.lexer.lineno += t.value.count("\n")

def t_error(t):
    print("Caracter No soporteado '%s'" % t.value[0])
    errors.append(Excepcion("lexico", "Caracter " + t.value[0] + " no pertenece al lenguaje. ", t.lexer.lineno, find_column(input, t)))
    t.lexer.skip(1)

def find_column(inp, token):
    line_start = inp.rfind('\n', 0, token.lexpos) + 1
    return (token.lexpos - line_start) + 1


import ply.lex as lex
lexer = lex.lex()

precedence = (
    ('left', 'eor'), 
    ('left', 'eand'),  
    ('right', 'unot'),
    ('left', 'eigualigual','ediferente', 'emenor', 'emenorigual', 'emayor', 'emayorigual'),
    ('left', 'esumar','eresta'), 
    ('left', 'emultiplicar', 'edivision', 'emodulo'),
    ('left', 'epotencia'),
    ('right', 'UMENOS'),
    ('left', 'eincremento', 'edecremento'),
    )

#gramatica
from Enums.Tipo import OperadorAritmetico, OperadorRelacional, OperadorLogico, TIPO
from Tabla_Simbolo.Excepcion import Excepcion
from Instrucciones.Imprimir import Imprimir
from Instrucciones.Declaracion import Declaracion
from Instrucciones.Asignacion import Asignacion
from Instrucciones.Pasos import Pasos
from Instrucciones.If import If
from Instrucciones.While import While
from Instrucciones.For import For
from Expresiones.Aritmetica import Aritmetica
from Expresiones.Identificador import Identificador
from Expresiones.Primitivos import Primitivos
from Expresiones.Relacional import Relacional
from Expresiones.Logica import Logica

def p_init(t):
    'INICIO            : LINSTRUCCIONES'
    t[0] = t[1]

def p_instrucciones_instrucciones_instruccion(t):
    'LINSTRUCCIONES    : LINSTRUCCIONES CUERPO'
    if t[2] != "":
        t[1].append(t[2])
    t[0] = t[1]

def p_instrucciones_instruccion(t):
    'LINSTRUCCIONES    : CUERPO'
    if t[1] == "":
        t[0] = []
    else:
        t[0] = [t[1]]

def p_instruccion(t):
    '''CUERPO      : IMPRIMIR    eptcoma
                   | DECLARACION eptcoma
                   | ASIGNACION  eptcoma
                   | INCREMENTALES eptcoma
                   | IFF
                   | WHILEE
                   | FORR
    '''
    t[0] = t[1]

def p_instruccionError(t):
    'CUERPO   : error eptcoma'
    errors.append(Excepcion("Sintactico", " En la instruccion " + str(t[1].value), t.lineno(1), find_column(input, t.slice[1])))
    t[0] = ""

def p_imprimir(t):
    'IMPRIMIR : eimprimir eparabre EXPRESION eparcierra'
    t[0] = Imprimir(t[3], t.lineno(1), find_column(input, t.slice[1]))

def p_declaracion(t):
    'DECLARACION  : TIPODATO id'
    t[0] = Declaracion(t[2], t.lineno(2), find_column(input, t.slice[2]), None, t[1])

def p_declaracion2(t):
    'DECLARACION  : TIPODATO id edspuntos EXPRESION'
    t[0] = Declaracion(t[2], t.lineno(2), find_column(input, t.slice[2]), t[4])

def p_asignacion(t):
    'ASIGNACION    : id edspuntos EXPRESION'
    t[0] = Asignacion(t[1], t[3], t.lineno(1), find_column(input, t.slice[1]))

def p_incrementales(t):
    '''INCREMENTALES   : id eincremento
                        | id edecremento'''
    if t[2] == "++":   t[0] = Pasos(t[1], OperadorAritmetico.MASMAS, t.lineno(1), find_column(input, t.slice[1]))
    elif t[2] == "--": t[0] = Pasos(t[1], OperadorAritmetico.MENOSMENOS, t.lineno(1), find_column(input, t.slice[1]))

def p_if1(t):
    '''IFF : eif edspuntos ellaabre CONDICIONN ecoma CUERPOSENTENCIA ellacierra'''
    t[0] = If(t[4], t[6], None, None, t.lineno(1), find_column(input, t.slice[1]))

def p_ifElse(t):
    '''IFF : eif edspuntos ellaabre CONDICIONN ecoma CUERPOSENTENCIA ellacierra eelse edspuntos ellaabre CUERPOSENTENCIA ellacierra'''
    t[0] = If(t[4], t[6], t[11], None, t.lineno(1), find_column(input, t.slice[1]))

def p_ifElseIf(t):
    '''IFF : eif edspuntos ellaabre CONDICIONN ecoma CUERPOSENTENCIA ellacierra eelse IFF'''
    t[0] = If(t[4], t[6], None, t[9], t.lineno(1), find_column(input, t.slice[1]))

def p_condiciones(t):
    'CONDICIONN : econd edspuntos EXPRESION'
    t[0] = t[3]

def p_cuerpoSentencia(t):
    'CUERPOSENTENCIA : einstr edspuntos ellaabre LINSTRUCCIONES ellacierra ' 
    t[0] = t[4]

def p_instruccionWhile(t):
    '''WHILEE  : ewhile edspuntos ellaabre CONDICIONN  ecoma CUERPOSENTENCIA ellacierra'''
    t[0] = While(t[4], t[6], t.lineno(1), find_column(input, t.slice[1]))

def p_for_instr(t):
    '''FORR : efor edspuntos ellaabre econd edspuntos eparabre EXP1 eptcoma EXPRESION eptcoma EXP3 eparcierra ecoma CUERPOSENTENCIA ellacierra'''
    t[0] = For(t[7], t[9], t[11], t[14], t.lineno(1), find_column(input, t.slice[1]))

def p_expresion1For(t):
    '''EXP1     : DECLARACION
                | ASIGNACION
    '''
    t[0] = t[1]

def p_expresion3For(t):
    '''EXP3    : INCREMENTALES
               | ASIGNACION'''
    t[0] = t[1]

def p_tipo(t):
    '''TIPODATO    :   eint
                   |   edouble
                   |   eboolean
                   |   echar
                   |   estring
    '''
    if t[1].lower() == 'int':      t[0] = TIPO.ENTERO
    elif t[1].lower() == 'double': t[0] = TIPO.DECIMAL
    elif t[1].lower() == 'boolean':t[0] = TIPO.BOOLEAN
    elif t[1].lower() == 'char':   t[0] = TIPO.CARACTER
    elif t[1].lower() == 'string': t[0] = TIPO.CADENA

def p_expresion_binaria(t):
    '''
    EXPRESION : EXPRESION esumar EXPRESION
            | EXPRESION eresta EXPRESION
            | EXPRESION emultiplicar EXPRESION
            | EXPRESION edivision EXPRESION
            | EXPRESION epotencia EXPRESION
            | EXPRESION emodulo EXPRESION
            | EXPRESION eigualigual EXPRESION
            | EXPRESION ediferente EXPRESION
            | EXPRESION emenor EXPRESION
            | EXPRESION emayor EXPRESION
            | EXPRESION emenorigual EXPRESION
            | EXPRESION emayorigual EXPRESION
            | EXPRESION eor EXPRESION
            | EXPRESION eand EXPRESION
    '''

    if t[2] == '+':    t[0] = Aritmetica(OperadorAritmetico.MAS, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == '-':  t[0] = Aritmetica(OperadorAritmetico.MENOS, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == '**': t[0] = Aritmetica(OperadorAritmetico.POTENCIA, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == '*':  t[0] = Aritmetica(OperadorAritmetico.MULTIPLICAR, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == '/':  t[0] = Aritmetica(OperadorAritmetico.DIVIDIR, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == '%':  t[0] = Aritmetica(OperadorAritmetico.MODULO, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == '==': t[0] = Relacional(OperadorRelacional.IGUALIGUAL, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == '!=': t[0] = Relacional(OperadorRelacional.DIFERENTE, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == '<':  t[0] = Relacional(OperadorRelacional.MENORQUE, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == '>':  t[0] = Relacional(OperadorRelacional.MAYORQUE, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == '<=': t[0] = Relacional(OperadorRelacional.MENORIGUAL, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == '>=': t[0] = Relacional(OperadorRelacional.MAYORIGUAL, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == '&&': t[0] = Logica(OperadorLogico.AND, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == '||': t[0] = Logica(OperadorLogico.OR, t[1], t[3], t.lineno(2), find_column(input, t.slice[2]))

def p_expresion_unaria(t):
    ''' EXPRESION : eresta EXPRESION %prec UMENOS
                  | not EXPRESION %prec unot
    '''
    if t[1] == '-':    t[0] = Aritmetica(OperadorAritmetico.UMENOS, t[2], None, t.lineno(1), find_column(input, t.slice[1]))
    elif t[1] == '!':  t[0] = Logica(OperadorLogico.NOT, t[2], None, t.lineno(1), find_column(input, t.slice[1]))

def p_expresion_agrupar(t):
    'EXPRESION : eparabre EXPRESION eparcierra'
    t[0] = t[2]

def p_expresion_unoenuno(t):
    '''EXPRESION :  EXPRESION eincremento
                 |  EXPRESION edecremento
    '''
    if t[2] == "++":   t[0] = Aritmetica(OperadorAritmetico.MASMAS, t[1], None, t.lineno(2), find_column(input, t.slice[2]))
    elif t[2] == "--": t[0] = Aritmetica(OperadorAritmetico.MENOSMENOS, t[1], None, t.lineno(2), find_column(input, t.slice[2]))

#llamar un identificador
def p_expresion_identificador(t):
    '''EXPRESION : id'''
    t[0] = Identificador(t[1], t.lineno(1), find_column(input, t.slice[1]))

#primitivos
def p_expresion_entero(t):
    '''EXPRESION : eentero'''
    t[0] = Primitivos(TIPO.ENTERO, t[1], t.lineno(1), find_column(input, t.slice[1]))

def p_primitivo_decimal(t):
    '''EXPRESION : edecimal'''
    t[0] = Primitivos(TIPO.DECIMAL, t[1], t.lineno(1), find_column(input, t.slice[1]))

def p_primitivo_cadena(t):
    '''EXPRESION : ecadena'''
    t[0] = Primitivos(TIPO.CADENA, str(t[1]), t.lineno(1), find_column(input, t.slice[1]))

def p_primitivo_caracter(t):
    '''EXPRESION : ecaracter'''
    t[0] = Primitivos(TIPO.CARACTER, str(t[1]), t.lineno(1), find_column(input, t.slice[1]))

def p_primitivo_true(t):
    '''EXPRESION : etrue'''
    t[0] = Primitivos(TIPO.BOOLEAN, True, t.lineno(1), find_column(input, t.slice[1]))

def p_primitivo_false(t):
    '''EXPRESION : efalse'''
    t[0] = Primitivos(TIPO.BOOLEAN, False, t.lineno(1), find_column(input, t.slice[1]))

def p_primitivo_null(t):
    '''EXPRESION : enull'''
    t[0] = Primitivos(TIPO.NULO, str(t[1]), t.lineno(1), find_column(input, t.slice[1]))

import ply.yacc as yacc
parser = yacc.yacc()


input = ''

def getErrores():
    return errors


# FUNCION PARA REALIZAR GRAMTICA
def parse(inp) :
    global errors
    global lexer
    global parser
    errors = []
    lexer = lex.lex()
    parser = yacc.yacc()
    global input
    input = inp
    return parser.parse(inp)

def realizar_dot(astTree):
    inicio_dot = NodoArbol("RAIZ")
    instruccion_dot = NodoArbol("INSTRUCCIONES")
    for instruccion_ast in astTree.getInstrucciones():
        instruccion_dot.addHijoNodo(instruccion_ast.getNodo())
    inicio_dot.addHijoNodo(instruccion_dot)
    grafo = astTree.getDot(inicio_dot) 
    dirname = os.path.dirname(__file__)
    direcc = os.path.join(dirname, 'ast.dot')
    file = open(direcc, "w+", encoding="utf-8")
    file.write(grafo)
    file.close()
    os.system('dot -T svg -o ast.svg ast.dot')
    os.system('dot -T pdf -o ast.pdf ast.dot')


def grammar_analisis(entrada, txtWidget):
    from Tabla_Simbolo.Arbol import Arbol
    from Tabla_Simbolo.TablaSimbolos import TablaSimbolos
    lista_variables.clear()

    instrucciones = parse(entrada)      
    astTree = Arbol(instrucciones)      
    Tabla_Global = TablaSimbolos()      
    astTree.setTSglobal(Tabla_Global)
    astTree.setTextoInterfaz(txtWidget) 
 
    for error in errors:
        astTree.getExcepciones().append(error)
        astTree.updateConsola(error.toString())

    for instruccion in astTree.getInstrucciones():

        if isinstance(instruccion, Declaracion) or isinstance(instruccion, Asignacion) or isinstance(instruccion,Imprimir) or isinstance(instruccion,While) or isinstance(instruccion,For) or isinstance(instruccion,If):
            value_asig_decla = instruccion.ejecutar(astTree, Tabla_Global)

            if isinstance(value_asig_decla, Excepcion):
                astTree.getExcepciones().append(value_asig_decla)
                astTree.updateConsola(value_asig_decla.toString())

    print(astTree.getExcepciones())
    print(astTree.getConsola())
    return astTree


