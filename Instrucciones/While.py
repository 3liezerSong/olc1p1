from Ambito.Instrucciones import Instrucciones
from Tablas_simbolos.Error import Error
from Enums.TipoValor import TIPO
from Tablas_simbolos.TablaSimbolos import TablaSimbolos

class While(Instrucciones): # while (true) {xxxxx}
    def __init__(self, _condicion, _instrucciones, _fila, _columna):
        self.condicion = _condicion
        self.instrucciones = _instrucciones
        self.fila = _fila
        self.columna = _columna

    def ejecutar(self, arbol, tabla):
        while True:
            condicion = self.condicion.ejecutar(arbol, tabla)
            if isinstance(condicion, Error): 
                return condicion
            # Verificamos las codiciones while(true){xxxxxx} nuevo ambito si es true
            elif self.condicion.tipo == TIPO.BOOLEAN:
                if bool(condicion) is True:
                    nuevaTabla = TablaSimbolos(tabla)
                    for instruccion in self.instrucciones:
                        resultado = instruccion.interpretar(arbol, nuevaTabla)
                        if isinstance(resultado, Error) :
                            arbol.getExcepciones().append(resultado)
                            arbol.updateConsola(resultado.toString())
                else:
                    # Romper estructura si es falso
                    break
            else:
                return Error(">Semantico", "Tipo de dato no booleano en While", self.fila, self.columna)
