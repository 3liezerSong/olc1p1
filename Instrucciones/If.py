from Ambito.Instrucciones import Instrucciones
from Tablas_simbolos.Error import Error
from Enums.TipoValor import TIPO
from Tablas_simbolos.TablaSimbolos import TablaSimbolos

class If(Instrucciones):
    def __init__(self, _condicion, _instruccionesIf, _instruccionesElse, _elseIf, _fila, _columna):
        self.condicion = _condicion
        self.instruccionesIf = _instruccionesIf
        self.instruccionesElse = _instruccionesElse
        self.elseIf = _elseIf
        self.fila = _fila
        self.columna = _columna

    def ejecutar(self, arbol, tabla):
        condicion = self.condicion.ejecutar(arbol,tabla)
        if isinstance(condicion,Error):
            return condicion
        elif self.condicion.tipo == TIPO.BOOLEAN:
            if bool(condicion) == True:
                #creamos el ambito para el if
                nuevaTabla = TablaSimbolos(tabla)
                for instruccion in self.instruccionesIf: # if(true){xxxxxxx}
                    resultado = instruccion.ejecutar(arbol,nuevaTabla)
                    if isinstance(resultado, Error): # si el resultado es tipo error entonces..
                        arbol.getExcepciones().append(resultado)
                        arbol.updateConsola(resultado.toString())
                    else:
                        None
            elif self.instruccionesElse != None:
                nuevaTabla = TablaSimbolos(tabla) # Ambito para if(false){...}else{xxxxxx}
                for instruccion in self.instruccionesElse:
                    resultado = instruccion.ejecutar(arbol,nuevaTabla)
                    if isinstance(resultado, Error): # si el resultado es tipo error entonces..
                        arbol.getExcepciones().append(resultado)
                        arbol.updateConsola(resultado.toString())

            elif self.elseIf != None:
                resultado = self.elseIf.ejecutar(arbol, tabla)
                if isinstance(resultado, Error): 
                    return resultado
            else:
                pass
            
        else:
            return Error("Semantico", "Tipo de dato no booleano en IF", self.fila, self.columna)
