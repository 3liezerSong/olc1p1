from Ambito.Instrucciones import Instrucciones
from Tablas_simbolos.Error import Error
from Enums.TipoValor import TIPO

class Imprimir(Instrucciones):
    def __init__(self, _expresion, _fila, _columna): # {expresion,fila,columna}
        self.expresion = _expresion
        self.fila = _fila
        self.columna = _columna

    def ejecutar(self, arbol, tabla):
        valorImprimir = self.expresion.ejecutar(arbol,tabla) #retorna el valor de primitivos etc
        if isinstance(valorImprimir, Error): # verifica que no sea instancia de tipo error
            return valorImprimir # #{tipo,descripcion,fila,columna}
        elif self.expresion.tipo == TIPO.NULO:
            return Error("Semantico", "No se puede imprimir un Nulo", self.fila, self.columna)
        else:
            pass
        arbol.updateConsola(valorImprimir)
        return None

