from Ambito.Instrucciones import Instrucciones
from Ambito.Arbol import NodoArbol
from Ambito.Simbolo import Simbolo
from Tablas_simbolos.Error import Error
from Enums.TipoValor import TIPO
from Tabla_Simbolo.TablaSimbolos import lista_variables


class Declaracion(Instrucciones):
    def __init__(self, _tipo ,_identificador, _fila, _columna, _expresion=None):
        self.tipo = _tipo
        self.identificador = _identificador
        self.expresion = _expresion
        self.fila = _fila
        self.columna = _columna

    def ejecutar(self, arbol, tabla):
        if self.expresion is not None:
             # Valor a asignar a la variable
            valor = self.expresion.ejecutar(arbol, tabla)
            if self.tipo == self.expresion.tipo:
                 pass
            else:
                return Error(">Semantico", "Declaracion con tipo de expresion incorrecta", self.fila, self.columna)
            if isinstance(valor, Error): return valor
        else:
            valor = "null"

        simbolo = Simbolo(str(self.identificador), self.tipo, self.fila, self.columna, valor)
        resultado = tabla.setTabla(simbolo)

        if isinstance(resultado, Excepcion): return resultado
        
        return None

