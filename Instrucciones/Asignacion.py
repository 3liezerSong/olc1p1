from Ambito.Instrucciones import Instruciones
#from Abstract.NodoArbol import NodoArbol
from Tablas_simbolos.Error import Error
from Ambito.Simbolo import Simbolo

class Asignacion(Instrucciones):
    def __init__(self, _identificador, _expresion, _fila, _columna):
        self.identificador = _identificador
        self.expresion = _expresion
        self.fila = _fila
        self.columna = _columna

    def ejecutar(self, arbol, tabla):
        valorAsignacion = self.expresion.ejecutar(arbol, tabla)
        if isinstance(valorAsignacion, Error): 
            return valorAsignacion
        simbolo = Simbolo(self.identificador, self.expresion.tipo, self.fila, self.columna, valorAsignacion)
        resultado = tabla.actualizarTabla(simbolo)

        if isinstance(resultado, Error): 
            return resultado
        return None
