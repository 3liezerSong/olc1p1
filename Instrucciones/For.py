from Ambito.Instrucciones import Instrucciones
from Instrucciones.Asignacion import Asignacion
from Instrucciones.Asignacion import Declaracion
from Tablas_simbolos.Error import Error
from Enums.TipoValor import TIPO
from Tablas_simbolos.TablaSimbolos import TablaSimbolos


class For(Instrucciones):
    def __init__(self, _expresion1, _expresion2, _expresion3, _instrucciones, fila, columna):
        self.expresion1 = _expresion1
        self.expresion2 = _expresion2
        self.expresion3 = _expresion3
        self.instrucciones = _instrucciones
        self.fila = _fila
        self.columna = _columna

    def ejecutar(self, arbol, tabla):
        if isinstance(self.expresion1, Asignacion):
            nuevaTabla = tabla
        elif isinstance(self.expresion1,Declaracion):
            nuevaTabla = TablaSimbolos(tabla)   
        else:
            return Error("Semantico", "Necesita Declaracion o Asignacion en For", self.fila, self.columna)
        #Realizar operatividad de la expresion1
        valorExp1 = self.expresion1.ejecutar(arbol,nuevaTabla)
        if isinstance(valorExp1,Error):
            return valorExp1
        else: 
            pass

        while True:
            condicion = self.expresion2.ejecutar(arbol,nuevaTabla)
            if isinstance(condicion, Error):
                return condicion
            elif self.expresion2.tipo == TIPO.BOOLEAN:
                if bool(condicion) is True: # Verificamos las codiciones fo(int i = 0,xxx,i++)
                    nuevaTabla = TablaSimbolos(nuevaTabla)
                    for instruccion in self.instrucciones:
                        resultado = instruccion.ejecutar(arbol, nuevaTabla)  # EJECUTA INSTRUCCION ADENTRO DEL IF
                        if isinstance(resultado,Error):
                            tree.getExcepciones().append(resultado)
                            tree.updateConsola(result.toString())
                    valorExp3 = self.expresion3.ejecutar(arbol, nuevaTabla)
                    if isinstance(valorExp3, Error): return valorExp3
                else:
                    break #Romper estructura si expresion2 es falsa
            else:
                return Error("Semantico", "Tipo de dato no booleano en for", self.fila, self.columna)
